# My CKE Button

## Introduction

My CKE Button makes it possible to create custom button styles, each having 6 different colors
for text, background and border, and their respective hover versions.

Other than colors, you can also set the border radius, border width and padding of each style.

It's also possible with this module to create one-time styles.

## Requirements

 * CKEditor 4 (_drupal/ckeditor_ package)

My CKE Button requires CKEditor 4 to function. As of _2022.10.12._ CKEditor 4 is considered a  
contributed module, and will become EOL (end-of-life) in _2023. november_.

No external libraries are required.

## Installation

 1. Install the module normally at `/admin/modules`, as you would do with any other module
 2. Add the newly available **Add button** and **Remove button** buttons to one of your
    text formats with CKEditor enabled at `/admin/config/content/formats`

## Configuration

To create new button styles, visit `/admin/config/content/myckebutton-styles`.

Each row contains a style, which has 6 different colors, in 3 groups,...:

 * Text colors
  * Text color
  * Text hover color
 * Background colors
  * Background color
  * Background hover color
 * Border colors
  * Border color
  * Border hover color

...and 3 properties, with the value and it's unit being stored separately:

 * Border radius
  * Value
  * Unit
 * Padding
  * Value
  * Unit
 * Border width
  * Value
  * Unit
    
A preview button is rendered for each row, so you can see the changes when editing a style.
    
## Usage

 1. Edit a piece of content with a text field using your previously edited text format
 2. Create and/or select a link inside CKEditor and click on the "_Create button_" button, or
    right click to open the contextual menu inside the editor and press the _Create button_ menu item
 3. Select a pre-defined style from the _Styles_ list, or create a custom style
 4. Press **OK** to apply the style

## Limitations

 * Previewing percentage values on the preview button provides unrealistic scenarios, as these
   percentages are based on the container of the button
 * Hover effects cannot be tested inside CKEditor, because CKEditor filters out inline Javascript to 
   prevent unexpected crashes and erros inside the editor instance