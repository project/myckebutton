<?php

namespace Drupal\myckebutton\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the My CKE Button configuration form.
 */
class MyCKEButtonConfigForm extends ConfigFormBase {
  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'ckeditor.plugin.myckebutton';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'myckebutton_buttonstyles';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    //Getting the styles from the configuration
    $config = $this->config(static::SETTINGS);
    $styles = $config->get('styles');

    //Trying to access the loaded styles (new and old alike)
    $loaded_styles = $form_state->get('loaded_styles');

    //If the loaded styles cannot be accessed (when opening the page for example), we create a list from the existing configuration
    if (empty($loaded_styles)) {
      //In case there are no built in styles, create an empty row, otherwise just load them
      if (empty($styles)) {
        $form_state->set('loaded_styles', [false]);
      } else {
        $form_state->set('loaded_styles', $styles);
      }

      $loaded_styles = $form_state->get('loaded_styles');
    }

    $wrapper_id = 'myckebutton-ajax-wrapper';

    //Loading the preview buttons and the custom CSS
    $form['#attached']['library'][] = 'myckebutton/myckebutton.form';

    $form['#tree'] = true;

    $form['style_list'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Style'),
        $this->t('Operations'),
        ['data' => $this->t('Is built in'), 'class' => 'hidden'],
      ],
      '#empty' => $this->t('No button styles have been created yet!'),
      '#prefix' => '<div id="myckebutton-ajax-wrapper">',
      '#suffix' => '</div>',
      '#input' => false,
    ];

    /**
     * Each loaded style will generate it's own row.
     * Non-default styles cannot be edited or removed
     */
    foreach ($loaded_styles as $key => $style) {
      //Whether the current style in built in, or not
      $built_in = $style['is-built-in'] ?? false;

      //Each option's value is the same as it's label, so the value can be easily saved
      $units = [
        'px' => 'px',
        '%' => '%',
        'in' => 'in',
        'cm' => 'cm',
        'mm' => 'mm',
        'em' => 'em',
        'ex' => 'ex',
        'pt' => 'pt',
        'pc' => 'pc',
      ];

      //One row in the table
      $row = &$form['style_list'][$key];

      //A collection of all the fieldsets, containing the colors and properties
      $row['style_definition'] = [
        '#type' => 'fieldset',
        '#attributes' => [
          'class' => ['myckebutton-style-definition'],
        ],
      ];

      //The actual values of the style
      $style_def = &$row['style_definition'];

      //Style name
      $style_def['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#default_value' => $style['name'] ?? '',
        '#size' => 30,
        '#required' => true,
        '#disabled' => $built_in,
      ];

      //Style color collection (text, background, border)
      $style_def['colors'] = [
        '#type' => 'fieldset',
        '#attributes' => [
          'class' => ['myckebutton-fieldset', 'myckebutton-container'],
        ],
      ];

      $colors = &$style_def['colors'];

      //Text colors
      $colors['text_colors'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Text colors'),
        '#attributes' => [
          'class' => ['myckebutton-fieldset', 'myckebutton-fieldset-inner'],
        ],
      ];

      $text_colors = &$colors['text_colors'];

      $text_colors['text_color'] = [
        '#type' => 'color',
        '#title' => $this->t('Regular'),
        '#default_value' => $style['style']['text-color'] ?? '#000000',
        '#disabled' => $built_in,
      ];

      $text_colors['text_hover_color'] = [
        '#type' => 'color',
        '#title' => $this->t('Hover'),
        '#default_value' => $style['style']['text-hover-color'] ?? '#000000',
        '#disabled' => $built_in,
      ];

      //Background colors
      $colors['background_colors'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Background colors'),
        '#attributes' => [
          'class' => ['myckebutton-fieldset', 'myckebutton-fieldset-inner'],
        ],
      ];

      $background_colors = &$colors['background_colors'];

      $background_colors['background_color'] = [
        '#type' => 'color',
        '#title' => $this->t('Regular'),
        '#default_value' => $style['style']['background-color'] ?? '#000000',
        '#disabled' => $built_in,
      ];

      $background_colors['background_hover_color'] = [
        '#type' => 'color',
        '#title' => $this->t('Hover'),
        '#default_value' => $style['style']['background-hover-color'] ?? '#000000',
        '#disabled' => $built_in,
      ];

      //Border colors
      $colors['border_colors'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Border colors'),
        '#attributes' => [
          'class' => ['myckebutton-fieldset', 'myckebutton-fieldset-inner'],
        ],
      ];

      $border_colors = &$colors['border_colors'];

      $border_colors['border_color'] = [
        '#type' => 'color',
        '#title' => $this->t('Regular'),
        '#default_value' => $style['style']['border-color'] ?? '#000000',
        '#disabled' => $built_in,
      ];

      $border_colors['border_hover_color'] = [
        '#type' => 'color',
        '#title' => $this->t('Hover'),
        '#default_value' => $style['style']['border-hover-color'] ?? '#000000',
        '#disabled' => $built_in,
      ];

      //Style properties (border radius/width, padding)
      $style_def['properties'] = [
        '#type' => 'fieldset',
        '#attributes' => [
          'class' => ['myckebutton-fieldset', 'myckebutton-container'],
        ],
      ];

      $properties = &$style_def['properties'];

      //Border radius value and unit
      $properties['border_radius_data'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Border radius'),
        '#attributes' => [
          'class' => ['myckebutton-fieldset', 'myckebutton-fieldset-inner'],
        ],
      ];

      $border_radius = &$properties['border_radius_data'];

      $border_radius['border_radius'] = [
        '#type' => 'number',
        '#title' => $this->t('Value'),
        '#min' => 0,
        '#step' => 1,
        '#default_value' => intval($style['style']['border-radius']) ?? 0,
        '#disabled' => $built_in,
      ];

      $border_radius['border_radius_unit'] = [
        '#type' => 'select',
        '#title' => $this->t('Unit'),
        '#options' => $units,
        '#default_value' => $style['style']['border-radius-unit'] ?? 'px',
        '#disabled' => $built_in,
      ];

      //Padding value and unit
      $properties['padding_data'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Padding'),
        '#attributes' => [
          'class' => ['myckebutton-fieldset', 'myckebutton-fieldset-inner'],
        ],
      ];

      $padding = &$properties['padding_data'];

      $padding['padding'] = [
        '#type' => 'number',
        '#title' => $this->t('Value'),
        '#min' => 0,
        '#step' => 1,
        '#default_value' => intval($style['style']['padding']) ?? 0,
        '#disabled' => $built_in,
      ];

      $padding['padding_unit'] = [
        '#type' => 'select',
        '#title' => $this->t('Unit'),
        '#options' => $units,
        '#default_value' => $style['style']['padding-unit'] ?? 'px',
        '#disabled' => $built_in,
      ];

      //Border width value and unit
      $properties['border_width_data'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Border width'),
        '#attributes' => [
          'class' => ['myckebutton-fieldset', 'myckebutton-fieldset-inner'],
        ],
      ];

      $border_width = &$properties['border_width_data'];

      $border_width['border_width'] = [
        '#type' => 'number',
        '#title' => $this->t('Value'),
        '#min' => 0,
        '#step' => 1,
        '#default_value' => intval($style['style']['border-width']) ?? 0,
        '#disabled' => $built_in,
      ];

      //Border width does not support percentage values, so the "%" option has to be removed
      $units_no_percentage = $units;
      unset($units_no_percentage['%']);

      $border_width['border_width_unit'] = [
        '#type' => 'select',
        '#title' => $this->t('Unit'),
        '#options' => $units_no_percentage,
        '#default_value' => $style['style']['border-width-unit'] ?? 'px',
        '#disabled' => $built_in,
      ];

      //Remove style button in each row, 
      $row['remove_row'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => 'remove_value_' . $key,
        '#style_index' => $key,
        '#submit' => ['::removeStyleSubmit'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::valuesAjax',
          'event' => 'click',
          'wrapper' => $wrapper_id,
        ],
        '#disabled' => $built_in,
      ];

      //Hidden field value to determine during form submit that if a style is built in
      $row['is_built_in'] = [
        '#type' => 'hidden',
        '#value' => strval($built_in),
      ];
    }

    //Add style button under the list of styles
    $form['add_style'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add new style'),
      '#submit' => ['::addStyleSubmit'],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::valuesAjax',
        'wrapper' => $wrapper_id,
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Ajax callback for value operations.
   */
  public function valuesAjax(array &$form, FormStateInterface $form_state) {
    return $form['style_list'];
  }

  /**
   * Submit callback for adding a new style.
   */
  public function addStyleSubmit(array &$form, FormStateInterface $form_state) {
    $loaded_styles = $form_state->get('loaded_styles');
    //A new row is basically just a false value 
    $loaded_styles[] = false;

    $form_state->set('loaded_styles', $loaded_styles);
    $form_state->setRebuild();
  }

  /**
   * Submit callback for removing a style.
   */
  public function removeStyleSubmit(array $form, FormStateInterface $form_state) {
    $style_index = $form_state->getTriggeringElement()['#style_index'];
    $loaded_styles = $form_state->get('loaded_styles');

    unset($loaded_styles[$style_index]);
    $form_state->set('loaded_styles', $loaded_styles);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    //Get the actual values from the form, also create an empty array for all style names
    $styles = $form_state->getValue('style_list');
    $names = [];

    foreach ($styles as $key => $row) {
      //Load each style's name into the array
      $style_name = $row['style_definition']['name'];
      $names[$key] = $style_name;

      $element_selector = 'style_list][' . $key . '][style_definition][name';

      //All style names have to start with an uppercase letter, otherwise send an error message
      if (preg_match('/^$|^\s+$/', $style_name)) {
        $form_state->setErrorByName($element_selector, $this->t('Empty style names are not allowed!'));
      } else if (!ctype_upper(substr($style_name, 0, 1))) {
        $form_state->setErrorByName($element_selector, $this->t('Style names have to start with an uppercase letter!'));
      }
    }

    //Make an array by counting each name to determine their uniqueness
    $counted_names = array_count_values($names);

    //If there are duplicates, send an error message
    foreach ($counted_names as $name => $count) {
      if ($count > 1) {
        $indexes = array_keys($names, $name);

        foreach ($indexes as $index) {
          $form_state->setErrorByName('style_list][' . $index . '][style_definition][name', $this->t('This name is already in use!'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    //The existing config
    $config = $this->config('ckeditor.plugin.myckebutton');

    //Form values
    $styles = $form_state->getValue('style_list');

    //Creating an empty array for the new styles
    $new_styles = [];

    //We create a new entry from each style inside the form
    foreach ($styles as $row) {
      $style_definition = &$row['style_definition'];
      $new_style = &$new_styles[];

      //Style name and whether it is built in or not
      $new_style['name'] = $style_definition['name'];
      $new_style['is-built-in'] = boolval($row['is_built_in']);

      //Text colors
      $new_style['style']['text-color'] = $style_definition['colors']['text_colors']['text_color'];
      $new_style['style']['text-hover-color'] = $style_definition['colors']['text_colors']['text_hover_color'];

      //Background colors
      $new_style['style']['background-color'] = $style_definition['colors']['background_colors']['background_color'];
      $new_style['style']['background-hover-color'] = $style_definition['colors']['background_colors']['background_hover_color'];

      //Border colors
      $new_style['style']['border-color'] = $style_definition['colors']['border_colors']['border_color'];
      $new_style['style']['border-hover-color'] = $style_definition['colors']['border_colors']['border_hover_color'];

      //Border radius value and unit
      $new_style['style']['border-radius'] = strval($style_definition['properties']['border_radius_data']['border_radius']);
      $new_style['style']['border-radius-unit'] = $style_definition['properties']['border_radius_data']['border_radius_unit'];

      //Padding value and unit
      $new_style['style']['padding'] = strval($style_definition['properties']['padding_data']['padding']);
      $new_style['style']['padding-unit'] = $style_definition['properties']['padding_data']['padding_unit'];

      //Border width value and unit
      $new_style['style']['border-width'] = strval($style_definition['properties']['border_width_data']['border_width']);
      $new_style['style']['border-width-unit'] = $style_definition['properties']['border_width_data']['border_width_unit'];
    }

    //Sort all the new styles by name
    usort($new_styles, function ($a, $b) {
      return strcmp($a['name'], $b['name']);
    });

    //Replace the existing configuration with the new values
    $config->set('styles', $new_styles)->save();
    parent::submitForm($form, $form_state);
  }
}
