<?php

namespace Drupal\myckebutton\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "myckebutton" plugin. This file exposes our custom plugin to CKEditor and providing it the list of pre-defined button styles.
 *
 * @CKEditorPlugin(
 *   id = "myckebutton",
 *   label = @Translation("My CKE button")
 * )
 */
class MyCKEButton extends CKEditorPluginBase {
  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getModulePath('myckebutton') . '/libraries/plugins/myckebutton/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return ['myckebutton/myckebutton.admin'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = \Drupal::config('ckeditor.plugin.myckebutton');
    $button_styles = $config->get('styles');

    return [
      'buttonStyles' => $button_styles,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'MyCKEButtonAdd' => [
        'label' => $this->t('Create button'),
        'image' => $this->getModulePath('myckebutton') . '/libraries/plugins/myckebutton/icons/myckebuttonadd.png',
      ],
      'MyCKEButtonRemove' => [
        'label' => $this->t('Remove button'),
        'image' => $this->getModulePath('myckebutton') . '/libraries/plugins/myckebutton/icons/myckebuttonremove.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCssFiles(Editor $editor) {
    return [
      $this->getModulePath('myckebutton') . '/css/plugins/myckebutton/myckebutton.css',
    ];
  }
}
