/**
 * @file 
 * Generates preview buttons for each styles in the My CKE Button configuration form.
 */

(function ($, Drupal) {
  'use strict';

  /**
   * Changes the supplied button's styling, based on the available input fields.
   * 
   * @param {jQuery} $button
   *   The button to be modified.
   * @param {jQuery} $colors
   *   All the color inputs (text, background and border + hover variants) available to the button.
   * @param {jQuery} $properties
   *   All the properties (border radius, padding and border width) available to the button.
   * @param {jQuery} $propertyUnits
   *   The unit of each property.
   */
  function applyCss($button, $colors, $properties, $propertyUnits) {
    $button.css('color', $colors[0].value);
    $button.css('backgroundColor', $colors[2].value);
    $button.css('borderColor', $colors[4].value);
    $button.attr(
      'onmouseout',
      "this.style.color='" + $colors[0].value + "';this.style.backgroundColor='" + $colors[2].value + "';this.style.borderColor='" + $colors[4].value + "';"
    );
    $button.attr(
      'onmouseover',
      "this.style.color='" + $colors[1].value + "';this.style.backgroundColor='" + $colors[3].value + "';this.style.borderColor='" + $colors[5].value + "';"
    );
    $button.css('borderRadius', $properties[0].value + $propertyUnits[0].value);
    $button.css('padding', $properties[1].value + $propertyUnits[1].value);
    $button.css('borderWidth', $properties[2].value + $propertyUnits[2].value);
  }
  /**
   * Attaches the myckebutton behavior to the My CKE Button configuration form.
   *
   * @type {Drupal~behavior}
   * 
   * @prop {Drupal~behaviorAttach} attach
   *   This function generates the buttons for each style row, providing a preview of said button style (percentage values will behave differently on production pages). It also attaches the "changeCss" function to each input field, so the changes can be seen immediately.
   */
  Drupal.behaviors.myckebutton = {
    attach: function (context) {
      $('.myckebutton-style-definition .js-form-type-textfield', context).each(function () {
        //Check if the button has already been created
        if (!$('.myckebutton', this).length) {
          //If the preview button doesn't exist, create it and append it to the row
          var buttonWrapper = document.createElement('div');
          var buttonAnchor = document.createElement('a');
          buttonWrapper.classList.add('myckebutton');
          buttonAnchor.attributes.href = '#';
          buttonAnchor.textContent = Drupal.t('Test button');
          buttonWrapper.appendChild(buttonAnchor);
          this.append(buttonWrapper);
        }
      });

      /**
       * For each button style row, the button, the colors and properties are collected to pass them to the applyCss function
       *
       * @todo Find a better way to store each jQuery object, preferably only their current values, inside a collection.
       */
      $('.myckebutton-style-definition', context).each(function () {
        var $button = $('.myckebutton a', this);
        var $colors = $('input[type=color]', this);
        var $properties = $('input[type=number]', this);
        var $propertyUnits = $('select', this);

        //Initial values for the preview button
        applyCss($button, $colors, $properties, $propertyUnits);

        //Apply the new css on firing any of the inputs' change event
        $('input:not([type=text]),select', context).each(function () {
          $(this).on('input', function () {
            applyCss($button, $colors, $properties, $propertyUnits);
          })
        });
      });
    }
  };
})(jQuery, Drupal);