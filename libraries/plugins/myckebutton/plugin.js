/**
 * @file The plugin file for the My CKE Button module, only usable with the module.
 *
 * @todo Create the documentation for this file
 */

(function ($, Drupal, CKEDITOR) {
	CKEDITOR.plugins.add('myckebutton', {
		icons: 'myckebuttonadd,myckebuttonremove',
		init: function (editor) {
			let pluginDirectory = this.path;

			editor.addCommand('createMyckebutton', new CKEDITOR.dialogCommand('myckebuttonDialog', {
				contextSensitive: true,
				startDisabled: true,

				refresh: function refresh(editor, path) {
					let element = path.lastElement && path.lastElement.getAscendant('a', true);

					if (element && element.getName() === 'a') {
						this.setState(CKEDITOR.TRISTATE_OFF);
					} else {
						this.setState(CKEDITOR.TRISTATE_DISABLED);
					}
				}
			}));

			editor.addCommand('removeMyckebutton', {
				contextSensitive: true,
				startDisabled: true,
				requiredContent: new CKEDITOR.style({
					element: 'div',
					attributes: { class: 'myckebutton' },
				}),

				exec: function (editor) {
					let element = editor.getSelection().getStartElement();

					if (element.hasAscendant('a')) {
						element = element.getAscendant('a', true);
					}

					if (element.getName() == 'a') {
						if (element.hasAttribute('onmouseover')) {
							element.removeAttribute('onmouseover');
						}

						if (element.hasAttribute('data-cke-pa-onmouseover')) {
							element.removeAttribute('data-cke-pa-onmouseover');
						}

						if (element.hasAttribute('onmouseout')) {
							element.removeAttribute('onmouseout');
						}

						if (element.hasAttribute('data-cke-pa-onmouseout')) {
							element.removeAttribute('data-cke-pa-onmouseout');
						}

						element.removeAttribute('style');
					}

					let divStyle = new CKEDITOR.style({
						element: 'div',
						attributes: { class: 'myckebutton' },
						alwaysRemoveElement: true
					});

					editor.removeStyle(divStyle);
				},

				refresh: function refresh(editor, path) {
					let element = path.lastElement && path.lastElement.getAscendant('a', true);

					if (
						element && element.getName() === 'a' &&
						(element.getAttribute('data-cke-pa-onmouseover') ||
							element.getAttribute('data-cke-pa-onmouseout')) &&
						element.getChildCount()
					) {
						this.setState(CKEDITOR.TRISTATE_OFF);
					} else {
						this.setState(CKEDITOR.TRISTATE_DISABLED);
					}
				}
			});

			CKEDITOR.dialog.add('myckebuttonDialog', pluginDirectory + 'dialogs/myckebutton.js');

			if (editor.ui.addButton) {
				editor.ui.addButton('MyCKEButtonAdd', {
					label: Drupal.t('Create button'),
					command: 'createMyckebutton'
				});
				editor.ui.addButton('MyCKEButtonRemove', {
					label: Drupal.t('Remove button'),
					command: 'removeMyckebutton'
				});
			}

			if (editor.contextMenu) {
				editor.addMenuGroup('myckebuttonGroup');

				editor.addMenuItem('createButton', {
					label: Drupal.t('Create button'),
					command: 'createMyckebutton',
					group: 'myckebuttonGroup',
					order: 1
				});

				editor.addMenuItem('removeButton', {
					label: Drupal.t('Remove button'),
					command: 'removeMyckebutton',
					group: 'myckebuttonGroup',
					order: 2
				});

				editor.contextMenu.addListener(function (element, selection, path) {
					let anchorElement = path.lastElement && path.lastElement.getAscendant('a', true);

					if (anchorElement && anchorElement.getName() === 'a') {
						if ((anchorElement.getAttribute('data-cke-pa-onmouseover') ||
							anchorElement.getAttribute('data-cke-pa-onmouseout')) &&
							anchorElement.getChildCount()) {
							return {
								createButton: CKEDITOR.TRISTATE_OFF,
								removeButton: CKEDITOR.TRISTATE_OFF
							}
						} else {
							return {
								createButton: CKEDITOR.TRISTATE_OFF,
								removeButton: CKEDITOR.TRISTATE_DISABLED
							}
						}
					} else {
						return {
							createButton: CKEDITOR.TRISTATE_DISABLED,
							removeButton: CKEDITOR.TRISTATE_DISABLED
						}
					}
				});
			}
		}
	});
})(jQuery, Drupal, CKEDITOR);
