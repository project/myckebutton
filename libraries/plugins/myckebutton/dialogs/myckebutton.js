/**
 * @file Dialog command file for the My CKE Button plugin file.
 * CKEditor 4 lacks native support for color inputs introduced in HTML5,
 * so currently these inputs are created from raw html markup.
 *
 * @todo Create the rest documentation for this file
 */

CKEDITOR.dialog.add('myckebuttonDialog', function (editor) {
	var styleListLabel = Drupal.t('Styles'),
		txtLabel = Drupal.t('Text color'),
		txtId = 'txt-color',
		txtHvrLabel = Drupal.t('Text hover color'),
		txtHvrId = 'txt-hvr-color',
		bgLabel = Drupal.t('Background color'),
		bgId = 'bg-color',
		bgHvrLabel = Drupal.t('Background hover color'),
		bgHvrId = 'bg-hvr-color',
		brdLabel = Drupal.t('Border color'),
		brdId = 'brd-color',
		brdHvrLabel = Drupal.t('Border hover color'),
		brdHvrId = 'brd-hvr-color',
		brdRadiusId = Drupal.t('Border radius'),
		pdngId = Drupal.t('Padding'),
		brdWidthId = Drupal.t('Border width');

	var buttonStyles = editor.config.buttonStyles;
	var styleNames = [];
	var styleLabels = [];

	buttonStyles.forEach(styleDefinition => {
		styleNames.push([styleDefinition.name]);
		styleLabels.push(styleDefinition.name);
	});

	styleNames.push([Drupal.t('Custom'), 'custom']);

	function applyStyle(dialogObject, selectedStyle) {
		var loadedStyle;

		buttonStyles.forEach(styleDefinition => {
			if (styleDefinition.name === selectedStyle) {
				loadedStyle = styleDefinition.style;
				return;
			}
		});

		document.getElementById(txtId).value = loadedStyle['text-color'];
		document.getElementById(bgId).value = loadedStyle['background-color'];
		document.getElementById(brdId).value = loadedStyle['border-color'];
		document.getElementById(txtHvrId).value = loadedStyle['text-hover-color'];
		document.getElementById(bgHvrId).value = loadedStyle['background-hover-color'];
		document.getElementById(brdHvrId).value = loadedStyle['border-hover-color'];
		dialogObject.getContentElement('tab-standard-settings', 'brd-radius').setValue(loadedStyle['border-radius']);
		dialogObject.getContentElement('tab-standard-settings', 'brd-radius-unit').setValue(loadedStyle['border-radius-unit']);
		dialogObject.getContentElement('tab-standard-settings', 'padding').setValue(loadedStyle['padding']);
		dialogObject.getContentElement('tab-standard-settings', 'padding-unit').setValue(loadedStyle['padding-unit']);
		dialogObject.getContentElement('tab-standard-settings', 'brd-width').setValue(loadedStyle['border-width']);
		dialogObject.getContentElement('tab-standard-settings', 'brd-width-unit').setValue(loadedStyle['border-width-unit']);
	}

	function setToCustom(dialogObject) {
		dialogObject.getContentElement('tab-standard-settings', 'btn-pre').setValue('custom');
	}

	return {
		title: Drupal.t('Button creation/customization'),
		minWidth: 400,
		minHeight: 200,

		contents: [
			{
				id: 'tab-standard-settings',
				label: Drupal.t('Standard Settings'),
				elements: [
					{
						type: 'select',
						id: 'btn-pre',
						label: styleListLabel,
						items: styleNames,
						default: 'custom',

						onChange: function (event) {
							var selectedStyle = event.data.value;

							if (event.sender.type === 'select' && selectedStyle !== 'custom') {
								var dialog = this.getDialog();
								applyStyle(dialog, selectedStyle);
							}
						},

						setup: function (element) {
							var styleName = element.getAttribute('data-selected-style');

							if (styleName && styleLabels.includes(styleName)) {
								var dialog = this.getDialog();
								applyStyle(dialog, styleName);
								this.setValue(styleName);
							}
						},
					},
					{
						type: 'hbox',
						children: [
							{
								type: 'vbox',
								width: 200,
								children: [
									{
										type: 'html',
										id: txtId,
										label: txtLabel,
										html:
											`
                                            <div>
                                                <label id="txt-color-label">` + txtLabel + `</label>
                                                <input class="cke_myckebutton_color" id="` + txtId + `" type="color" aria-labelledby="txt-color-label">
                                            </div>
                                        `,

										onInput: function (event) {
											if (event.sender.type !== 'select') {
												var dialog = this.getDialog();
												setToCustom(dialog);
											}
										},

										setup: function (element) {
											var event = element.getAttribute('data-cke-pa-onmouseout');

											if (event) {
												var eventString = event.split(';');
												document.getElementById(txtId).value = eventString[0].slice(eventString[0].indexOf("#"), -1);
											} else {
												document.getElementById(txtId).value = '#000000';
											}
										},

										commit: function (element) {
											element.setStyle('color', document.getElementById(txtId).value);
										}
									},
									{
										type: 'html',
										id: bgId,
										label: bgLabel,
										html:
											`
                                            <div>
                                                <label id="bg-color-label">` + bgLabel + `</label>
                                                <input class="cke_myckebutton_color" id="` + bgId + `" type="color" aria-labelledby="bg-color-label">
                                            </div>
                                        `,

										onInput: function (event) {
											if (event.sender.type !== 'select') {
												var dialog = this.getDialog();
												setToCustom(dialog);
											}
										},

										setup: function (element) {
											var event = element.getAttribute('data-cke-pa-onmouseout');

											if (event) {
												var eventString = event.split(';');
												document.getElementById(bgId).value = eventString[1].slice(eventString[1].indexOf("#"), -1);
											} else {
												document.getElementById(bgId).value = '#000000';
											}
										},

										commit: function (element) {
											element.setStyle('background-color', document.getElementById(bgId).value);
										}
									},
									{
										type: 'html',
										id: brdId,
										label: brdLabel,
										html:
											`
                                            <div>
                                                <label id="brd-color-label">` + brdLabel + `</label>
                                                <input class="cke_myckebutton_color" id="` + brdId + `" type="color" aria-labelledby="brd-color-label">
                                            </div>
                                        `,

										onInput: function (event) {
											if (event.sender.type !== 'select') {
												var dialog = this.getDialog();
												setToCustom(dialog);
											}
										},

										setup: function (element) {
											var event = element.getAttribute('data-cke-pa-onmouseout');

											if (event) {
												var eventString = event.split(';');
												document.getElementById(brdId).value = eventString[2].slice(eventString[2].indexOf("#"), -1);
											} else {
												document.getElementById(brdId).value = '#000000';
											}
										},

										commit: function (element) {
											element.setStyle('border-color', document.getElementById(brdId).value);
										}
									},
								]
							},
							{
								type: 'vbox',
								width: 200,
								children: [
									{
										type: 'html',
										id: txtHvrId,
										label: txtHvrLabel,
										html:
											`
                                            <div>
                                                <label id="txt-color-hover-label">` + txtHvrLabel + `</label>
                                                <input class="cke_myckebutton_color" id="` + txtHvrId + `" type="color" aria-labelledby="txt-color-hover-label">
                                            </div>
                                        `,

										onInput: function (event) {
											if (event.sender.type !== 'select') {
												var dialog = this.getDialog();
												setToCustom(dialog);
											}
										},

										setup: function (element) {
											var event = element.getAttribute('data-cke-pa-onmouseover');

											if (event) {
												var eventString = event.split(';');
												document.getElementById(txtHvrId).value = eventString[0].slice(eventString[0].indexOf("#"), -1);
											} else {
												document.getElementById(txtHvrId).value = '#000000';
											}
										}
									},
									{
										type: 'html',
										id: bgHvrId,
										label: bgHvrLabel,
										html:
											`
                                            <div>
                                                <label id="bg-color-hover-label">` + bgHvrLabel + `</label>
                                                <input class="cke_myckebutton_color" id="` + bgHvrId + `" type="color" aria-labelledby="bg-color-hover-label">
                                            </div>
                                        `,

										onInput: function (event) {
											if (event.sender.type !== 'select') {
												var dialog = this.getDialog();
												setToCustom(dialog);
											}
										},

										setup: function (element) {
											var event = element.getAttribute('data-cke-pa-onmouseover');

											if (event) {
												var eventString = event.split(';');
												document.getElementById(bgHvrId).value = eventString[1].slice(eventString[1].indexOf("#"), -1);
											} else {
												document.getElementById(bgHvrId).value = '#000000';
											}
										}
									},
									{
										type: 'html',
										id: brdHvrId,
										label: brdHvrLabel,
										html:
											`
                                            <div>
                                                <label id="brd-color-hover-label">` + brdHvrLabel + `</label>
                                                <input class="cke_myckebutton_color" id="` + brdHvrId + `" type="color" aria-labelledby="brd-color-hover-label">
                                            </div>
                                        `,

										onInput: function (event) {
											if (event.sender.type !== 'select') {
												var dialog = this.getDialog();
												setToCustom(dialog);
											}
										},

										setup: function (element) {
											var event = element.getAttribute('data-cke-pa-onmouseover');

											if (event) {
												var eventString = event.split(';');
												document.getElementById(brdHvrId).value = eventString[2].slice(eventString[2].indexOf("#"), -1);
											} else {
												document.getElementById(brdHvrId).value = '#000000';
											}
										}
									}
								]
							}
						]
					},
					{
						type: 'hbox',
						className: 'myckebutton_properties',
						children: [
							{
								type: 'hbox',
								widths: ['100px', 'unset'],
								children: [
									{
										type: 'text',
										id: 'brd-radius',
										width: '80px',
										label: brdRadiusId,
										validate: CKEDITOR.dialog.validate.regex(/^[0-9]*$/, Drupal.t('Numbers only!')),

										onInput: function (event) {
											var inputElement = document.getElementById(this.domId).getElementsByClassName('cke_dialog_ui_input_text')[1];
											if (inputElement === document.activeElement) {
												var dialog = this.getDialog();
												setToCustom(dialog);
											}
										},

										setup: function (element) {
											if (element.getStyle('border-radius')) {
												this.setValue(element.getStyle('border-radius').replace(/\D*/g, ''));
											}
										}
									},
									{
										type: 'select',
										id: 'brd-radius-unit',
										items: [['px'], ['%'], ['in'], ['cm'], ['mm'], ['em'], ['ex'], ['pt'], ['pc']],
										'default': 'px',

										onChange: function (event) {
											var inputElement = document.getElementById(this.domId).getElementsByClassName('cke_dialog_ui_input_select')[1];
											if (inputElement === document.activeElement) {
												var dialog = this.getDialog();
												setToCustom(dialog);
											}
										},

										setup: function (element) {
											if (element.getStyle('border-radius')) {
												this.setValue(element.getStyle('border-radius').replace(/\d*/g, ''));
											}
										}
									}
								]
							},
							{
								type: 'hbox',
								widths: ['100px', 'unset'],
								children: [
									{
										type: 'text',
										id: 'padding',
										width: '80px',
										label: pdngId,
										validate: CKEDITOR.dialog.validate.regex(/^[0-9]*$/, Drupal.t('Numbers only!')),

										onInput: function (event) {
											var inputElement = document.getElementById(this.domId).getElementsByClassName('cke_dialog_ui_input_text')[1];
											if (inputElement === document.activeElement) {
												var dialog = this.getDialog();
												setToCustom(dialog);
											}
										},

										setup: function (element) {
											if (element.getStyle('padding')) {
												this.setValue(element.getStyle('padding').replace(/\D*/g, ''));
											}
										}
									},
									{
										type: 'select',
										id: 'padding-unit',
										items: [['px'], ['%'], ['in'], ['cm'], ['mm'], ['em'], ['ex'], ['pt'], ['pc']],
										'default': 'px',

										onChange: function (event) {
											var inputElement = document.getElementById(this.domId).getElementsByClassName('cke_dialog_ui_input_select')[1];
											if (inputElement === document.activeElement) {
												var dialog = this.getDialog();
												setToCustom(dialog);
											}
										},

										setup: function (element) {
											if (element.getStyle('padding')) {
												this.setValue(element.getStyle('padding').replace(/\d*/g, ''));
											}
										}
									}
								]
							}
						]
					},
					{
						type: 'hbox',
						widths: ['100px', 'unset'],
						className: 'myckebutton_properties',
						children: [
							{
								type: 'text',
								id: 'brd-width',
								width: '80px',
								label: brdWidthId,
								validate: CKEDITOR.dialog.validate.regex(/^[0-9]*$/, Drupal.t('Numbers only!')),

								onInput: function (event) {
									var inputElement = document.getElementById(this.domId).getElementsByClassName('cke_dialog_ui_input_text')[1];
									if (inputElement === document.activeElement) {
										var dialog = this.getDialog();
										setToCustom(dialog);
									}
								},

								setup: function (element) {
									if (element.getStyle('border-width')) {
										this.setValue(element.getStyle('border-width').replace(/\D*/g, ''));
									}
								}
							},
							{
								type: 'select',
								id: 'brd-width-unit',
								items: [['px'], ['in'], ['cm'], ['mm'], ['em'], ['ex'], ['pt'], ['pc']],
								'default': 'px',

								onChange: function (event) {
									var inputElement = document.getElementById(this.domId).getElementsByClassName('cke_dialog_ui_input_select')[1];
									if (inputElement === document.activeElement) {
										var dialog = this.getDialog();
										setToCustom(dialog);
									}
								},

								setup: function (element) {
									if (element.getStyle('border-width')) {
										this.setValue(element.getStyle('border-width').replace(/\d*/g, ''));
									}
								}
							}
						]
					}
				]
			}
		],

		onShow: function () {
			var selection = editor.getSelection();
			var element = selection.getStartElement();
			var wrapperElement = element.getParent();

			//If our selection is a My CKE Button instance, then expose it to the dialog...
			if (wrapperElement.getName() == 'div' && wrapperElement.hasClass('myckebutton')) {
				this.wrapperElement = wrapperElement;
				var activeStyle = element.getAttribute('data-selected-style');

				//In case of deleting a style, but already using said style, remove it from the element
				if (activeStyle && !styleLabels.includes(activeStyle)) {
					element.removeAttribute(activeStyle);
					alert(Drupal.t('This button is using a non-existent style!'));
				}

				this.element = element;
				this.insertMode = false;
				//...if not, create one
			} else {
				wrapperElement = editor.document.createElement('div');
				wrapperElement.addClass('myckebutton');
				this.wrapperElement = wrapperElement;
				this.element = element;
				this.insertMode = true;
			}

			//Give a variable to the "setup" function inside the dialog elements
			this.setupContent(element);
		},

		onOk: function () {
			var dialog = this;
			var wrapperElement = dialog.wrapperElement,
				element = dialog.element;

			//Hover effects
			var elementMouseOut =
				"this.style.color='" + document.getElementById(txtId).value + "';" +
				"this.style.backgroundColor='" + document.getElementById(bgId).value + "';" +
				"this.style.borderColor='" + document.getElementById(brdId).value + "';";

			var elementMouseOver =
				"this.style.color='" + document.getElementById(txtHvrId).value + "';" +
				"this.style.backgroundColor='" + document.getElementById(bgHvrId).value + "';" +
				"this.style.borderColor='" + document.getElementById(brdHvrId).value + "';";

			element.setAttribute('data-cke-pa-onmouseout', elementMouseOut);
			element.setAttribute('data-cke-pa-onmouseover', elementMouseOver);

			//Assembling the different non-hover styles for the button (value + unit)
			element.setStyles({
				borderRadius: dialog.getValueOf('tab-standard-settings', 'brd-radius') + dialog.getValueOf('tab-standard-settings', 'brd-radius-unit'),
				padding: dialog.getValueOf('tab-standard-settings', 'padding') + dialog.getValueOf('tab-standard-settings', 'padding-unit'),
				borderWidth: dialog.getValueOf('tab-standard-settings', 'brd-width') + dialog.getValueOf('tab-standard-settings', 'brd-width-unit')
			});

			var selectedStyle = dialog.getValueOf('tab-standard-settings', 'btn-pre');

			if (selectedStyle !== 'custom') {
				element.setAttribute('data-selected-style', selectedStyle);
			} else {
				element.removeAttribute('data-selected-style');
			}

			dialog.commitContent(element);

			if (dialog.insertMode) {
				wrapperElement.append(element);
				editor.insertElement(wrapperElement);
			}
		},

		onCancel: function () {
			var dialog = this;
			delete dialog.wrapperElement;
			delete dialog.element;
		}
	}
});
